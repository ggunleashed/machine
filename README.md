# Machine

## Description

Machine is a Go package developed for reading data files from the game Go Gigantic by Motiga. However it is possible to use this package for other types of ini files.

## How to Download

To install this package, make sure that you have Go installed and that you have a GOPATH set. For instructions on installing and setting up Go, visit [the Go docs](https://golang.org/doc/install).

When you have set up Go, installing the machine package is really simple. Open your terminal or command line and use the following command:

```
go get gitlab.com/ggunleashed/machine
```

## Documentation
To be continued...
