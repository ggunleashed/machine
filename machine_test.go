package machine_test

import (
  "testing"
  "gitlab.com/ggunleashed/machine"
  "github.com/stretchr/testify/assert"
)

var correctPath = "testdata/test.ini"
var incorrectPath = "testdata/nonexisting.ini"

func TestReadIniWithIncorrectPath(t *testing.T) {
  iniData, err := machine.ReadIni(incorrectPath)
  assert.Error(t, err, "Expected error to return on opening non existing file: "+incorrectPath)
  assert.Nil(t, iniData, "Expected iniData to be nil on returned error")
}

func TestReadIniWithCorrectPath(t *testing.T) {
  output, err := machine.ReadIni(correctPath)

  assert.NoError(t, err, "Expected no error on correct path: "+correctPath)

  expectedIniData := make([]machine.IniData, 2)
  expectedIniData[0].Title = "[TestingTitle1"
  expectedIniData[0].Class = "TestingClass"
  expectedIniData[0].Data = make(map[string]string)
  expectedIniData[0].Data["Data1"] = "Value1"
  expectedIniData[0].Data["Data2"] = "Value2"

  expectedIniData[1].Title = ""
  expectedIniData[1].Class = "TestingClass"
  expectedIniData[1].Data = make(map[string]string)
  expectedIniData[1].Data["Data3"] = "Value3"
  expectedIniData[1].Data["Data4"] = "Value4"

  assert.Equal(t, expectedIniData, output, "Expected output to be the same as the expectedIniData")
}
