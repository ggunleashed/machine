package machine

import (
  "io/ioutil"
  "strings"
)

//IniData ...
type IniData struct {
  Title string
  Class string
  Data  map[string]string
}

//ReadIni returns IniData
func ReadIni(filePath string) (iniDataSlice []IniData, err error) {
  data, err := ioutil.ReadFile(filePath)
  if err != nil {
    return nil, err
  }
  splittedData := strings.Split(convertToUnicode(string(data)), "\n[")
  for i := range splittedData {
    iniDataSlice = append(iniDataSlice, dataToMapFromIni(splittedData[i]))
  }
  return iniDataSlice, nil
}

func dataToMapFromIni(data string) (iniData IniData) {
  iniData.Data = make(map[string]string)
  splittedData := strings.Split(strings.TrimSpace(data), "\n")
  for _, dataLine := range splittedData {
    dataLine = strings.TrimSpace(dataLine)
    if len(dataLine) > 1 {
      if (strings.EqualFold(dataLine[len(dataLine)-2:len(dataLine)-1], "]") && iniData.Title == "") || strings.EqualFold(dataLine[len(dataLine)-1:], "]") {
        dataLine = strings.TrimSuffix(dataLine, "]")
        ClassData := strings.Split(dataLine, " ")
        if len(ClassData) > 1 {
          iniData.Title = ClassData[0]
          iniData.Class = ClassData[1]
        } else {
          iniData.Title = ""
          iniData.Class = ClassData[0]
        }
      } else {
        if !strings.HasPrefix(dataLine, ";") {
          ClassData := strings.SplitN(dataLine, "=", 2)
          if len(ClassData) > 1 {
            iniData.Data[strings.TrimSpace(ClassData[0])] = strings.TrimSpace(ClassData[1])
          }
        }
      }
    }
  }
  return iniData
}

//ConvertToUnicode Changes all hexadecimal characters in the string
//to unicode versions of those characters
func convertToUnicode(data string) string {
  data = strings.Replace(data, "\x00", "", -1)
  data = strings.Replace(data, "\xa0", " ", -1)
  data = strings.Replace(data, "\xa1", "\u00a1", -1)
  data = strings.Replace(data, "\xbf", "\u00bf", -1)

  data = strings.Replace(data, "\xc0", "\u00c0", -1)
  data = strings.Replace(data, "\xc2", "\u00c2", -1)
  data = strings.Replace(data, "\xc3", "\u00c3", -1)
  data = strings.Replace(data, "\xc4", "\u00c4", -1)
  data = strings.Replace(data, "\xc5", "\u00c5", -1)
  data = strings.Replace(data, "\xc6", "\u00c6", -1)
  data = strings.Replace(data, "\xc7", "\u00c7", -1)
  data = strings.Replace(data, "\xc8", "\u00c8", -1)
  data = strings.Replace(data, "\xc9", "\u00c9", -1)
  data = strings.Replace(data, "\xca", "\u00ca", -1)
  data = strings.Replace(data, "\xcb", "\u00cb", -1)
  data = strings.Replace(data, "\xcc", "\u00cc", -1)
  data = strings.Replace(data, "\xcd", "\u00cd", -1)
  data = strings.Replace(data, "\xce", "\u00ce", -1)
  data = strings.Replace(data, "\xcf", "\u00cf", -1)

  data = strings.Replace(data, "\xd0", "\u00d0", -1)
  data = strings.Replace(data, "\xd1", "\u00d1", -1)
  data = strings.Replace(data, "\xd2", "\u00d2", -1)
  data = strings.Replace(data, "\xd3", "\u00d3", -1)
  data = strings.Replace(data, "\xd4", "\u00d4", -1)
  data = strings.Replace(data, "\xd5", "\u00d5", -1)
  data = strings.Replace(data, "\xd6", "\u00d6", -1)
  data = strings.Replace(data, "\xd7", "\u00d7", -1)
  data = strings.Replace(data, "\xd8", "\u00d8", -1)
  data = strings.Replace(data, "\xd9", "\u00d9", -1)
  data = strings.Replace(data, "\xda", "\u00da", -1)
  data = strings.Replace(data, "\xdb", "\u00db", -1)
  data = strings.Replace(data, "\xdc", "\u00dc", -1)
  data = strings.Replace(data, "\xdd", "\u00dd", -1)
  data = strings.Replace(data, "\xde", "\u00de", -1)
  data = strings.Replace(data, "\xdf", "\u00df", -1)

  data = strings.Replace(data, "\xe0", "\u00e0", -1)
  data = strings.Replace(data, "\xe1", "\u00e1", -1)
  data = strings.Replace(data, "\xe2", "\u00e2", -1)
  data = strings.Replace(data, "\xe3", "\u00e3", -1)
  data = strings.Replace(data, "\xe4", "\u00e4", -1)
  data = strings.Replace(data, "\xe5", "\u00e5", -1)
  data = strings.Replace(data, "\xe6", "\u00e6", -1)
  data = strings.Replace(data, "\xe7", "\u00e7", -1)
  data = strings.Replace(data, "\xe8", "\u00e8", -1)
  data = strings.Replace(data, "\xe9", "\u00e9", -1)
  data = strings.Replace(data, "\xea", "\u00ea", -1)
  data = strings.Replace(data, "\xeb", "\u00eb", -1)
  data = strings.Replace(data, "\xec", "\u00ec", -1)
  data = strings.Replace(data, "\xed", "\u00ed", -1)
  data = strings.Replace(data, "\xee", "\u00ee", -1)
  data = strings.Replace(data, "\xef", "\u00ef", -1)

  data = strings.Replace(data, "\xf0", "\u00f0", -1)
  data = strings.Replace(data, "\xf1", "\u00f1", -1)
  data = strings.Replace(data, "\xf2", "\u00f2", -1)
  data = strings.Replace(data, "\xf3", "\u00f3", -1)
  data = strings.Replace(data, "\xf4", "\u00f4", -1)
  data = strings.Replace(data, "\xf5", "\u00f5", -1)
  data = strings.Replace(data, "\xf6", "\u00f6", -1)
  data = strings.Replace(data, "\xf7", "\u00f7", -1)
  data = strings.Replace(data, "\xf8", "\u00f8", -1)
  data = strings.Replace(data, "\xf9", "\u00f9", -1)
  data = strings.Replace(data, "\xfa", "\u00fa", -1)
  data = strings.Replace(data, "\xfb", "\u00fb", -1)
  data = strings.Replace(data, "\xfc", "\u00fc", -1)
  data = strings.Replace(data, "\xfd", "\u00fd", -1)
  data = strings.Replace(data, "\xfe", "\u00fe", -1)
  data = strings.Replace(data, "\xff", "\u00ff", -1)

  data = strings.Replace(data, "\u00c3\x82\xa1", "\u00a1", -1)
  data = strings.Replace(data, "\u00c3\x82\xbf", "\u00bf", -1)
  data = strings.Replace(data, "\x81", "", -1)
  data = strings.Replace(data, "\u00c3\x80", "\u00c0", -1)
  data = strings.Replace(data, "\u00c3\x82", "\u00c2", -1)
  data = strings.Replace(data, "\x1c", "", -1)
  data = strings.Replace(data, "\x01", "", -1)
  data = strings.Replace(data, "\x1e", "", -1)

  data = strings.Replace(data, "\xc1", "\u00c1", -1)
  return data
}
